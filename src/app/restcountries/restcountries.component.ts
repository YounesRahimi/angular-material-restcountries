import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {startWith} from 'rxjs/operators';
import {CountryService} from "../country.service";

@Component({
  selector: 'app-restcountries-autocomplete',
  templateUrl: './restcountries.component.html',
  styleUrls: ['./restcountries.component.scss']
})
export class RestcountriesComponent implements OnInit {
  filteredCountries: Observable<{ name: String }[]> = of();

  loading = this.countryService.loading;

  countryForm = this.fb.group({
    country: [null, Validators.required]
  });
  private timeout: number = 0;

  constructor(private fb: FormBuilder,
              private countryService: CountryService) {
  }

  ngOnInit() {
    this.countryForm.controls["country"].valueChanges
      .pipe(
        startWith('iran'),
        // delay(500),
        // switchMap(value => this._filter(value)),
      ).subscribe(filterValue => {
      this.countryService.startLoading()
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => this._filter(filterValue), 700);
    });
  }

  private _filter(filterValue: string) {
    console.log(`Filtering ${filterValue}...`)
    this.filteredCountries = this.countryService.filterCountries(filterValue).pipe()
  }

  onSubmit(): void {
    alert('Thanks!');
  }
}
