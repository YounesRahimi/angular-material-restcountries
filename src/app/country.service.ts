import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, of, Subject} from "rxjs";
import {environment} from "../environments/environment";
import {catchError, map, tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class CountryService implements OnDestroy {

  constructor(private http: HttpClient) {
  }

  // Loading stream
  private readonly _loading = new Subject<boolean>();
  get loading(): Observable<boolean> {
    return this._loading;
  }

  startLoading() {
    this._loading.next(true)
  }

  filterCountries(userInput: String): Observable<{ name: String }[]> {
    return this.http.get<{ name: string }[]>(`${environment.serverUrl}/rest/v2/name/${userInput}`)
      .pipe(
        tap(() => this._loading.next(true)),
        // If we get to this point, we know we got the data,
        // set loading to false, return the result itself
        map((res) => {
          this._loading.next(false);
          return res;
        }),
        catchError(error => this.handleError(error, this))
      )
  }

  private handleError(error: HttpErrorResponse, that: CountryService): Observable<{ name: String }[]> {
    that._loading.next(false)
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else if (error.status === 404) {
      return of([]);
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return of([{name: 'Something bad happened; please try again later.'}]);
  }

  // Cleanup.
  ngOnDestroy() {
    this._loading.unsubscribe();
  }

}
